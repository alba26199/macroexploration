//
//  SceneManager.swift
//  HellCampus
//
//  Created by Felinda Gracia Lubis on 11/10/21.
//

import SpriteKit

class SceneManager {
    
    static let shared = SceneManager()
    
    enum SceneType: Int {
        case MainMenu = 0, Gameplay
    }
    
    func transition(fromScene: SKScene, toScene: SceneType, transition: SKTransition? = nil) {
        guard let scene = getScene(toScene) else {return}
        scene.scaleMode = .aspectFill

        if let transition = transition {
            fromScene.view?.presentScene(scene, transition: transition)
        } else {
            fromScene.view?.presentScene(scene)
        }
    }
    
    func getScene(_ sceneType: SceneType) -> SKScene? {
        switch sceneType {
        case .MainMenu:
            return SKScene(fileNamed: "MainMenuScene")
            
        case .Gameplay:
            return SKScene(fileNamed: "GameScene")
        }
    }
}
