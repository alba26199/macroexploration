//
//  GameScene.swift
//  Hell Campus
//
//  Created by Felinda Gracia Lubis on 04/10/21.
//

import SpriteKit
import GameplayKit

// This is classroom scene
class GameScene: SKScene, SKPhysicsContactDelegate {
    var oldPosition: CGPoint?
    var isPlaying = true
    var enemies = [EnemyNode]()
    var hero = HeroNode()
    let joystick = JoystickNode()
    var specialAttack = 0
    let heroSpeed: CGFloat = 0.1
    var updateTime: Double = 0.0
    var updateTime2: Double = 0.0
    var destination = [CGPoint]()
    
    enum NodesZPosition: CGFloat{
        case hero, joystick
    }
    
    override func didMove(to view: SKView) {
        view.showsPhysics = true
        self.view?.isMultipleTouchEnabled = true
        configureEnemy()
        let gameplayNode = GameplayNode(frame: self.frame, hero: hero, heroSpeed: heroSpeed)
        gameplayNode.zPosition = 10
        
        self.addChild(gameplayNode)
        self.addChild(hero)
        physicsWorld.contactDelegate = self
        
        
    }
    
    func configureEnemy() {
        for i in 0...3 {
            let enemy = EnemyNode(spawnPos: getRandomPoint(enemyId: i), path: i)
            enemy.name = "enemy\(i)"
            enemies.append(enemy)
            self.addChild(enemy)
            
            let wait = SKAction.wait (forDuration: 0.2)
            let run = SKAction.run{self.enemyAttack(enemyPos: enemy.position)}
            
            enemy.run(SKAction.repeatForever(SKAction.sequence([wait, run])))
        }
    }
    
    func bulletDirection(x:CGFloat,y:CGFloat) -> CGVector {
        var dx = CGFloat(0)
        var dy = CGFloat(0)
        if x == 0 && y == 0{
            dx = CGFloat(30)
            dy = CGFloat(0)
        } else {
            dx = CGFloat(x)
            dy = CGFloat(y)
        }
        let magnitude = sqrt(dx * dx + dy * dy)
        dx /= magnitude
        dy /= magnitude
        let vector = CGVector(dx: 20*dx, dy: 20*dy)
        return vector
    }
    
    func enemyAttack(enemyPos: CGPoint) {
        let bullet = SKSpriteNode(imageNamed: "dummyBtn")
        bullet.setScale(1)
        bullet.zPosition = -1
        bullet.position = enemyPos
        
        let action = SKAction.moveTo(x: self.size.width + 100, duration: 0.5)
        let actionDone = SKAction.removeFromParent()
        bullet.run(SKAction.sequence([action, actionDone]))
        bullet.size = CGSize(width: 30, height: 30)
        bullet.physicsBody = SKPhysicsBody(circleOfRadius: bullet.size.width / 2)
        bullet.physicsBody?.categoryBitMask = PhysicsCategory.EnemyAttack
        bullet.physicsBody?.affectedByGravity = false
        bullet.physicsBody?.isDynamic = false
        self.addChild(bullet)
        
        var dx = CGFloat (enemyPos.x)
        var dy = CGFloat (enemyPos.y)
        
        let magnitude = sqrt (dx * dx + dy * dy )
        dx /= magnitude
        dy /= magnitude
        
        let vector = CGVector (dx: 30.0 * dx, dy: 30.0 * dy)
        bullet.physicsBody?.applyImpulse(vector)
        
    }
    
    func autoAimToEnemy(container: [SKSpriteNode], heroPosition: CGPoint) -> [String:CGFloat] {
        var dx:CGFloat = 0
        var dy:CGFloat = 0
        var dxdy = [[String: CGFloat]]()
        for node in container{
            dx = node.position.x - heroPosition.x
            dy = node.position.y - heroPosition.y
            let arr = ["dx":dx, "dy":dy, "dxdy":hypot(dx, dy)]
            dxdy.append(arr)
        }
        let sortedArray = dxdy.sorted { $0["dxdy"] ?? .zero < $1["dxdy"] ?? .zero }
        return sortedArray[0]
    }
    
    func getRandomPoint(enemyId: Int) -> CGPoint {
        var x = CGFloat()
        let y = CGFloat.random(in: 100...frame.maxY-100)

        switch enemyId {
        case 0:
            x = CGFloat.random(in: 0...330)
        case 1:
            x = CGFloat.random(in: 331...660)
        case 2:
            x = CGFloat.random(in: 661...990)
        case 3:
            x = CGFloat.random(in: 991...frame.maxX)
        default:
            x = CGFloat.random(in: 0...frame.maxX)
        }
        
        return CGPoint(x: x, y: y)
    }
    
    func touchDown(atPoint pos : CGPoint) {
        for enemy in enemies {
            enemy.calculateHP(attackPoint: 10)
        }
    }
    
    func touchMoved(toPoint pos : CGPoint) {
    }
    
    func touchUp(atPoint pos : CGPoint) {
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        for t in touches {
            touchDown(atPoint: t.location(in: self))
        }
        let touch = touches.first
        let position = touch!.location(in: self)
        let touchedNode = self.atPoint(position)
        let bullet: SKSpriteNode = {
            let sprite = SKSpriteNode(imageNamed: "RightStateHero")
            sprite.name = "HeroBullet"
            sprite.size = CGSize(width: 30, height: 30)
            sprite.position = hero.position
            sprite.zPosition = 1
            sprite.physicsBody = SKPhysicsBody(texture: sprite.texture!, size: sprite.size)
            sprite.physicsBody?.affectedByGravity = false
            sprite.physicsBody?.allowsRotation = false
            sprite.physicsBody?.isDynamic = true
            sprite.physicsBody?.categoryBitMask = PhysicsCategory.HeroAttack
            sprite.physicsBody?.contactTestBitMask = PhysicsCategory.HeroContact
            sprite.physicsBody?.collisionBitMask = 0
            return sprite
        }()
        if let names = touchedNode.name{
            if names == "attackButton"{
                print(joystick.isIdle)
                self.addChild(bullet)
                specialAttack += 1
                if specialAttack%3 == 0{
                    bullet.texture = SKTexture(imageNamed: "dummyBtn")
                }
                if specialAttack == 9{
                    specialAttack = 0
                }
            }
        }
        let distance = hero.autoAimToEnemy(container: enemies, heroPosition: hero.position)
//        let dx = distance["dx"] ?? 0
//        let dy = distance["dy"] ?? 0
//        print(distance["dxdy"])
        var dx:CGFloat = 0
        var dy:CGFloat = 0
        if distance["dxdy"] ?? 0 > 400{
            dx = joystick.joystickX
            dy = joystick.joystickY
        } else{
            dx = distance["dx"] ?? 0
            dy = distance["dy"] ?? 0
        }
        bullet.physicsBody?.applyImpulse(hero.bulletDirection(x: dx, y: dy))
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        guard let nodeA = contact.bodyA.node else {return}
        guard let nodeB = contact.bodyB.node else {return}
        let sortedNode = [nodeA,nodeB].sorted { $0.name ?? "" < $1.name ?? ""}
        let firstNode = sortedNode[0]
        let secondNode = sortedNode[1]
        
        if firstNode.name == "HeroBullet" && secondNode.name == "enemy"{
            if specialAttack%3 == 0{
                if let explosion = SKEmitterNode(fileNamed: "Explosion"){
                    explosion.position = secondNode.position
                    let addEmmit = SKAction.run({self.addChild(explosion)})
                    let rmEmmit = SKAction.run({explosion.removeFromParent()})
                    let wait = SKAction.wait(forDuration: 1)
                    let act = SKAction.sequence([addEmmit,wait,rmEmmit])
                    self.run(act)
                }
            }
            firstNode.removeFromParent()
        }
        
    }
//    
//    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
//        for t in touches { self.touchMoved(toPoint: t.location(in: self)) }
//    }
//    
//    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
//        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
//    }
//    
//    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
//        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
//    }
//    
//    
    
    override func update(_ currentTime: TimeInterval) {
        if oldPosition != hero.position{
            hero.heroIdleAnimation()
        }
        for i in enemies.indices {
            if updateTime == 0 {
                updateTime = currentTime
            }
            
            if destination.count <= enemies.count {
                destination.append(getRandomPoint(enemyId: i))
            }
            
            if currentTime - updateTime > 2 {
                updateTime = currentTime
                updateTime2 = currentTime
                for j in enemies.indices {
                    destination[j] = getRandomPoint(enemyId: j)
                }
            }
            
            if !destination.isEmpty {
                if  currentTime - updateTime2 > 0.5 && !enemies[i].contains(destination[i]) {
                    enemies[i].moveEnemy(toDestination: destination[i])
                }
            }
        }
//         else if oldPosition == hero.position{
//
//        }
        oldPosition = hero.position
    }
}
