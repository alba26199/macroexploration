//
//  PhysicsCategory.swift
//  Hell Campus
//
//  Created by Felinda Gracia Lubis on 04/10/21.
//

import Foundation

struct PhysicsCategory {
    static let Hero: UInt32 = 0x1 << 0
    static let Enemy: UInt32 = 0x1 << 0
    static let MapBoundary: UInt32 = 0x1 << 1
    static let Obstacle: UInt32 = 0x1 << 2
    static let NonPlayer: UInt32 = 0x1 << 3
    static let EnemyAttack: UInt32 = 0x1 << 4
    static let HeroAttack: UInt32 = 0x1 << 5
    static let Item: UInt32 = 0x1 << 6
    static let HeroCollision: UInt32 = Hero | Enemy | MapBoundary | Obstacle | NonPlayer | EnemyAttack
    static let EnemyCollision: UInt32 = Hero | MapBoundary | Obstacle | HeroAttack
    static let HeroContact: UInt32 = Enemy | MapBoundary | Obstacle | NonPlayer | EnemyAttack | Item
}
