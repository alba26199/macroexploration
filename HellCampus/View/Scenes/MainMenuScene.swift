//
//  MainMenuScene.swift
//  HellCampus
//
//  Created by Felinda Gracia Lubis on 11/10/21.
//

import SpriteKit

class MainMenuScene: SKScene {
    
    var titleNode = SKShapeNode()
    var playButton = SKShapeNode()
    var sceneManager = SceneManager.shared
    
    override func didMove(to view: SKView) {
        titleNode = SKShapeNode(rectOf: CGSize(width: frame.width/2, height: frame.height/3.5))
        titleNode.fillColor = .red
        
        playButton = SKShapeNode(rectOf: CGSize(width: frame.width/3, height: 100))
        playButton.fillColor = .yellow
        
        
        titleNode.position = CGPoint(x: frame.width/2, y: frame.height/2 + 100)
        playButton.position = CGPoint(x: frame.width/2, y: titleNode.position.y - titleNode.frame.height)
        
        addChild(titleNode)
        addChild(playButton)
    }
    
    func touchDown(atPoint pos : CGPoint) {
        print("tap outside")
        if playButton.contains(pos) {
            print("tapped")
            sceneManager.transition(fromScene: self, toScene: .Gameplay, transition: SKTransition.fade(with: .black, duration: 2))
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches {
            touchDown(atPoint: t.location(in: self))
        }
    }
    
    
}
