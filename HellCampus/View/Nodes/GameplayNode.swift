//
//  GameplayNode.swift
//  HellCampus
//
//  Created by Felinda Gracia Lubis on 12/10/21.
//

import SpriteKit

class GameplayNode: SKNode {
    var attackButton: SKSpriteNode = {
        let node = SKSpriteNode(imageNamed: "dummyBtn")
        node.name = "attackButton"
        node.size = CGSize(width: 150, height: 150)
        
        return node
    }()
    var weapon1: SKSpriteNode = {
        let node = SKSpriteNode(imageNamed: "dummyBtn")
        node.size = CGSize(width: 75, height: 75)
        
        return node
    }()
    
    var weapon2: SKSpriteNode = {
        let node = SKSpriteNode(imageNamed: "dummyBtn2")
        node.size = CGSize(width: 75, height: 75)

        return node
    }()
    
    var joystick = JoystickNode()
    
    init(frame: CGRect, hero: HeroNode, heroSpeed: CGFloat) {
        super.init()
        attackButton.position = CGPoint(x: frame.width/1.2, y: frame.height/3.2)
        weapon1.position = CGPoint(x: attackButton.position.x-attackButton.size.width/1.5, y: attackButton.position.y+attackButton.size.height/1.5)
        weapon2.position = CGPoint(x: attackButton.position.x-attackButton.size.width/1.1, y: attackButton.position.y)
        self.addChild(weapon1)
        self.addChild(weapon2)
        self.addChild(attackButton)
        self.addChild(joystick.analogJoystick)
        joystick.setupJoy(hero: hero, heroSpeed: heroSpeed)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
