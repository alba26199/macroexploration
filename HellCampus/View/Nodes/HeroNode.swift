//
//  HeroNode.swift
//  Hell Campus
//
//  Created by Adhitya Laksamana Bayu Adrian on 05/10/21.
//
import Foundation
import SpriteKit

class HeroNode: SKSpriteNode{
    init(){
        let texture = SKTexture(imageNamed: "hero-animation_idle-1")
        super.init(texture: texture, color: .white, size: texture.size())
        self.name = "Heroes"
        self.position = CGPoint(x: UIScreen.main.bounds.width/2, y: UIScreen.main.bounds.height/2)
        self.physicsBody = SKPhysicsBody(texture: texture , size: texture.size())
//        self.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: self.frame.width, height: self.frame.height/2), center: CGPoint(x: 0, y: 40))
    
        physicsBody?.affectedByGravity = false
        physicsBody?.allowsRotation = false
        physicsBody?.isDynamic = false
        physicsBody?.categoryBitMask = PhysicsCategory.Hero
        physicsBody?.collisionBitMask = PhysicsCategory.HeroCollision
        physicsBody?.contactTestBitMask = PhysicsCategory.HeroContact
//        self.run(SKAction.repeatForever(animation))
    }
    func autoAimToEnemy(container: [SKSpriteNode], heroPosition: CGPoint) -> [String:CGFloat] {
        var dx:CGFloat = 0
        var dy:CGFloat = 0
        var dxdy = [[String: CGFloat]]()
        for node in container{
            dx = node.position.x - heroPosition.x
            dy = node.position.y - heroPosition.y
            let arr = ["dx":dx, "dy":dy, "dxdy":hypot(dx, dy)]
            dxdy.append(arr)
        }
        let sortedArray = dxdy.sorted { $0["dxdy"] ?? .zero < $1["dxdy"] ?? .zero }
        return sortedArray[0]
    }
    func bulletDirection(x:CGFloat,y:CGFloat) -> CGVector {
        var dx = CGFloat(0)
        var dy = CGFloat(0)
        if x == 0 && y == 0{
            dx = CGFloat(30)
            dy = CGFloat(0)
        }else{
            dx = CGFloat(x)
            dy = CGFloat(y)
        }
        let magnitude = sqrt(dx * dx + dy * dy)
        dx /= magnitude
        dy /= magnitude
        let vector = CGVector(dx: 20*dx, dy: 20*dy)
        return vector
    }
    
    func heroIdleAnimation(){
        let f1 = SKTexture.init(imageNamed: "hero-animation_idle-1")
        let f2 = SKTexture.init(imageNamed: "hero-animation_idle-2")
        let f3 = SKTexture.init(imageNamed: "hero-animation_idle-3")
        let f4 = SKTexture.init(imageNamed: "hero-animation_idle-4")
        let f5 = SKTexture.init(imageNamed: "hero-animation_idle-5")
        let frames:[SKTexture] = [f1,f2,f3,f4,f5]
        let animation = SKAction.animate(with: frames, timePerFrame: 0.12)
        self.run(SKAction.repeatForever(animation))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}






















//class HeroNode{
//    var heroNode:SKSpriteNode = {
//        var sprite = SKSpriteNode(imageNamed: "RightStateHero")
//        sprite.name = "Heros"
//        sprite.position = CGPoint(x: UIScreen.main.bounds.width/2, y: UIScreen.main.bounds.height/2)
//        sprite.physicsBody = SKPhysicsBody(texture: sprite.texture!, size: sprite.size)
//        sprite.physicsBody?.affectedByGravity = false
//        sprite.physicsBody?.allowsRotation = false
//        sprite.physicsBody?.isDynamic = true
//        sprite.physicsBody?.categoryBitMask = PhysicsCategory.Hero
//        sprite.physicsBody?.collisionBitMask = PhysicsCategory.HeroCollision
//        sprite.physicsBody?.contactTestBitMask = PhysicsCategory.HeroContact
//        return sprite
//    }()
//}
