//
//  EnemyNode.swift
//  Hell Campus
//
//  Created by Felinda Gracia Lubis on 04/10/21.
//

import SpriteKit

class EnemyNode: SKSpriteNode {
    
    let enemySpeed:CGFloat = 2.0
    var enemyHealthBar: SKSpriteNode = {
        let healthBar = SKSpriteNode(imageNamed: "life")
        healthBar.zPosition = 1
        healthBar.anchorPoint = CGPoint(x: 0, y: 0)
        return healthBar
    }()
    
    var health: CGFloat = 100.0 {
        didSet {
            self.enemyHealthBar.size = CGSize(width: health, height: 25)
        }
    }
    
    init(spawnPos: CGPoint, path: Int) {
        let texture = SKTexture(imageNamed: "RightStateHero")
        super.init(texture: texture, color: .white, size: texture.size())
        self.name = "enemy"
        self.position = spawnPos
        
        
        self.physicsBody = SKPhysicsBody(texture: texture, size: texture.size())
        physicsBody?.categoryBitMask = PhysicsCategory.Enemy
        physicsBody?.collisionBitMask = PhysicsCategory.EnemyCollision
        physicsBody?.affectedByGravity = false
        physicsBody?.allowsRotation = false
        enemyHealthBar.position = CGPoint(x: -(self.frame.width / 1.45), y: self.frame.height / 1.6)
        self.addChild(enemyHealthBar)
    }
    
    func calculateHP(attackPoint: CGFloat) -> CGFloat {
        health -= attackPoint
        
        if health == 0 {
            self.isHidden = true
        }
        
        return health
    }
    
    func getDestinationPoint() -> CGPoint {
        
        let x = CGFloat.random(in: self.position.x - 200...self.position.x + 200)
        let y = CGFloat.random(in: self.position.y - 200...self.position.y + 200)
        
        return CGPoint(x: x, y: y)
    }
    
    func moveEnemy(toDestination: CGPoint) {
        if toDestination.x > self.position.x {
            self.texture = SKTexture(imageNamed: "RightStateHero")
            
        } else {
            self.texture = SKTexture(imageNamed: "LeftStateHero")
        }
            
            let dx = toDestination.x -  self.position.x
            let dy = toDestination.y -  self.position.y
            
            let angle = atan2(dy, dx)
            let vx = cos(angle) * enemySpeed
            let vy = sin(angle) * enemySpeed
            
            self.position.x += vx
            self.position.y += vy
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

