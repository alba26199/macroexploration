//
//  JoystickNode.swift
//  Hell Campus
//
//  Created by Adhitya Laksamana Bayu Adrian on 05/10/21.
//

import Foundation
import SpriteKit

class JoystickNode{
    var joystickX: CGFloat = 0
    var joystickY: CGFloat = 0
    var isIdle:Bool = true
    var analogJoystick: AnalogJoystick = {
        let sprite = AnalogJoystick(diameter: 150, colors: nil, images: (substrate: UIImage(named: "jSubstrate"), stick: UIImage(named: "jStick")))
        sprite.position = CGPoint(x: 200, y: 250)
        sprite.name = "joystickAnalog"
        sprite.zPosition = 10
        return sprite
    }()
    func setupJoy(hero: SKSpriteNode, heroSpeed: CGFloat = 0.2){
        analogJoystick.trackingHandler = { [unowned self] data in
            hero.position = CGPoint(x: hero.position.x + (data.velocity.x * heroSpeed),
                                        y: hero.position.y + (data.velocity.y * heroSpeed))
            //                        monster.zRotation = data.angular
            joystickX = data.velocity.x
            joystickY = data.velocity.y
            if joystickX < 0{
                hero.xScale = 1
            }else if joystickX > 0{
                hero.xScale = -1
            }
        }
    }
}
